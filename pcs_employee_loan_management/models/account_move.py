from odoo import api, fields, models, _
from odoo.exceptions import UserError

MAP_INVOICE_TYPE_PARTNER_TYPE = {
    'out_invoice': 'customer',
    'out_refund': 'customer',
    'out_receipt': 'customer',
    'in_invoice': 'supplier',
    'in_refund': 'supplier',
    'in_receipt': 'supplier',
    'entry': 'general',
}
class AccountMove(models.Model):
    _inherit = 'account.move'

    loan_id = fields.Many2one('employee.loan')

    @api.model
    def default_get(self, fields):
        res = super(AccountMove, self).default_get(fields)
        if self.ref:
            employee_loan_id = self.env['employee.loan'].search([('name', '=', self.ref)])
            # print('Employee Loan ID 1', employee_loan_id.id)
            # print('Employee Loan ID 2', self.loan_id.id)
            if self.loan_id:
                res.update({
                    'line_ids': [
                        (1, self.id, {
                            'account_id' : self.env.ref('pcs_employee_loan_management.account_account_loan_data').id,
                            'partner_id': self.loan_id.partner_id.id,
                            'debit' : 0.0,
                            'credit' : self.loan_id.total_to_pay
                        }), 
                        (1, self.id,{
                            'account_id' : self.env.ref('pcs_employee_loan_management.account_account_loan_data').id,
                            'partner_id': self.loan_id.partner_id.id,
                            'debit' : self.loan_id.total_to_pay,
                            'credit' : 0.0
                        }) 
                    ]
                })
        return res
    
    # overriding this function
    @api.model
    def get_invoice_types(self, include_receipts=False):
        return ['entry', 'out_invoice', 'out_refund', 'in_refund', 'in_invoice'] + (include_receipts and ['out_receipt', 'in_receipt'] or [])

    def _get_creation_message(self):
        # OVERRIDE
        if not self.is_invoice(include_receipts=True):
            return super()._get_creation_message()
        return {
            'out_invoice': _('Invoice Created'),
            'out_refund': _('Credit Note Created'),
            'in_invoice': _('Vendor Bill Created'),
            'in_refund': _('Refund Created'),
            'out_receipt': _('Sales Receipt Created'),
            'in_receipt': _('Purchase Receipt Created'),
            'entry': _('Entry Receipt Created'),
        }[self.type]
    
    def action_invoice_loan_payment(self):
        # return self.env['account.payment']\
        #     .with_context(active_ids=self.ids, active_model='account.move', active_id=self.id)\
        #     .action_register_payment()
        if not self._context.get('active_ids', []):
            return ''
        print('Employee Loan ID ', self.loan_id.id)
        print('RU ERROR  False', self.type in self.get_invoice_types(False))
        print('RU ERROR  True', self.type in self.get_invoice_types(True))
        print("get_invoice_types False", ['out_invoice', 'out_refund', 'in_refund', 'in_invoice'] + (False and ['out_receipt', 'in_receipt'] or []))
        print("get_invoice_types True", ['out_invoice', 'out_refund', 'in_refund', 'in_invoice'] + (True and ['out_receipt', 'in_receipt'] or []))
        ctx = {
            'default_payment_type': 'inbound',
            'default_partner_type': 'customer',
            'default_partner_id': self.loan_id.partner_id.id,
            'default_amount' : self.loan_id.monthly_payment,
            'default_communication' : self.name,
        }
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'account.payment',
            'views': [(False, 'form')],
            'view_id': False,
            'target': 'new',
            'context': ctx,
        }
    
    # @api.model
    # def create(self, vals):    
    #     res = super(AccountMove, self).create(vals)
    #     print("XDDD", self.env['employee.loan'].search([('name', '=', self.ref)]))
    #     self.env['employee.loan'].search([('name', '=', self.ref)]).write({'state': 'installment'})
    #     return res