from odoo import api, fields, models, _
from odoo.exceptions import UserError
from datetime import datetime
from dateutil.relativedelta import relativedelta


class EmployeeLoan(models.Model):
    _name = 'employee.loan'
    _description = 'model for employee loan'

    name = fields.Char(string="Reference", readonly=True, required=True, copy=False, default='New')
    company_id = fields.Many2one('res.company', 'Company', required=True, index=True, readonly=True, default=lambda self: self.env.company)
    currency_id = fields.Many2one("res.currency", related='company_id.currency_id', string="Currency", readonly=True, required=True)
    loan_date = fields.Datetime(string='Loan Date',default=lambda self: fields.datetime.now())
    payment_term = fields.Integer(string='Payment Term(month)')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('wait', 'Need to be Approved'),
        ('installment', 'Installment'),
        ('done', 'Paid off'),
        ], string='Status', readonly=True, copy=False, index=True, tracking=3, default='draft')
    expiration_date = fields.Datetime(readonly=True, force_save=True)
    user_id = fields.Many2one(comodel_name="res.users", string="Loaner", readonly=True, default=lambda self: self.env.user)
    installment_ids = fields.One2many('employee.loan.installment', 'loan_id')

    def _get_loan_line(self):
        product_id = self.env.ref('pcs_employee_loan_management.product_product_data').id 
        return [(0, 0, {
            'loan_id' : self.id,
            'product_id': product_id,
            'quantity' : 1,
            'interest_rate' : 5.00,
            'is_flat_rate' : True
        })]

    loan_line = fields.One2many('employee.loan.line', 'loan_id', default=_get_loan_line)
    account_move_id = fields.One2many('account.move', 'loan_id')
    partner_id = fields.Many2one(
        'res.partner', string='Customer', readonly=True,
        required=True, index=True, default=lambda self: self.env.ref('base.main_partner').id )
    amount_loan = fields.Monetary(string='Loan Amount', states={'draft': [('readonly', False)], 'wait': [('readonly', True)], 'installment': [('readonly', True)], 'done': [('readonly', True)]}, store=True, tracking=5)
    underpayment = fields.Monetary(string='amount to be paid', readonly=True)
    time_pay_remaining = fields.Integer(string='Time Pay Remaining(Month)')
    payment_count = fields.Integer(compute="_compute_payment_count")
    total_to_pay = fields.Monetary(string='Total', readonly=True, force_save=True)
    monthly_payment = fields.Monetary(string='Monthly Payment', readonly=True, force_save=True)

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code(
                'employee.loan') or 'New'
        result = super(EmployeeLoan, self).create(vals)
        return result

    def action_confirm(self):
        self.update({
            'state': 'wait'
        })
    
    @api.onchange('payment_term')
    def get_expiration_date(self):
        current_date = fields.Datetime.from_string(self.loan_date)
        added_date = current_date + relativedelta(months=self.payment_term)
        self.expiration_date = added_date
        # self.update({'expiration_date': added_date})
    
    @api.onchange('payment_term', 'loan_line')
    def compute_payment(self):
        for line in self.loan_line:
            if line.amount_loan > 0:
                principal = line.amount_loan / self.payment_term
                if line.is_flat_rate:
                    self.monthly_payment = principal + ((line.interest_rate / 100)*line.amount_loan)
                    self.total_to_pay = self.monthly_payment * self.payment_term

    def action_approve(self):
            # record.update({
            #     'state': 'installment'
            # })
        ctx = {
            'default_company_id': self.env.company.id,
            'default_loan_id': self.id,
            # 'default_type': 'in_receipt',
            'default_ref': self.name,
            'default_line_ids' :[
                (0, 0, {
                    'account_id' : self.env.ref('pcs_employee_loan_management.account_account_loan_data').id,
                    'partner_id': self.partner_id.id,
                    'debit' : 0,
                    'credit' : self.total_to_pay
                }), 
                (0, 0,{
                    'account_id' : self.env.ref('pcs_employee_loan_management.account_account_loan_data').id,
                    'partner_id': self.partner_id.id,
                    'debit' : self.total_to_pay,
                    'credit' : 0
                }) 
            ],
            'default_journal_id' : self.env.ref('pcs_employee_loan_management.account_journal_data').id
        }
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'account.move',
            'views': [(False, 'form')],
            'view_id': False,
            'target': 'new',
            'context': ctx,
        }

class EmployeeLoanLine(models.Model):
    _name = 'employee.loan.line'
    _description = 'model for loan line'

    def _default_product_id(sefl):
        return self.env.ref('pcs_employee_loan_management.product_product_data').id 
    
    def _get_default_parent(self):
        parent_id = self.env.context.get('parent_id') 
        parent_model = self.env.context.get('parent_model')       
        if parent_id and parent_model:
            parent_obj = self.env[parent_model].browse(parent_id)
            # now you have the parent obj to do what you want
            default_value = parent_obj.id
            return default_value

    loan_id = fields.Many2one('employee.loan', required=True, index=True, copy=False)
    product_id = fields.Many2one('product.product', required=True, index=True, copy=False)

    quantity = fields.Integer(default=1, readonly=True)
    company_id = fields.Many2one('res.company', 'Company', required=True, index=True, readonly=True, default=lambda self: self.env.company)
    currency_id = fields.Many2one("res.currency", related='company_id.currency_id', string="Currency", readonly=True, required=True)
    amount_loan = fields.Monetary(string='Loan Amount', store=True, tracking=5,  required=True)
    interest_rate = fields.Float(string='Rate', default=5.00)
    is_flat_rate = fields.Boolean(string='Flat Rate', default=True, readonly=True)

class EmployeeLoanInstallment(models.Model):
    _name = 'employee.loan.installment'
    _description = 'model for loan installment'

    loan_id = fields.Many2one('employee.loan')
    company_id = fields.Many2one('res.company', 'Company', required=True, index=True, readonly=True, default=lambda self: self.env.company)
    currency_id = fields.Many2one("res.currency", related='company_id.currency_id', string="Currency", readonly=True, required=True)
    payment_id = fields.Many2one('account.payment')
    payment_date = fields.Datetime()
    payment_amount = fields.Monetary()