# -*- coding: utf-8 -*-
{
    'name': "PCS Employee Loan",

    'summary': """
    """,

    'description': """
    """,

    'author': "Port Cities",
    'website': "https://www.portcities.net",

    'category': 'Uncategorized',
    'version': '13.0.1.0',

    'depends': ['base', 'account_accountant', 'sale_management', 'product', 'purchase'],

    'data': [
        'data/account_journal_data.xml',
        'data/product_template_data.xml',
        'data/account_account_data.xml',
        'data/ir_sequence_data.xml',
        'security/res_groups.xml',
        'data/ir_rule_data.xml',
        'security/ir.model.access.csv',
        'views/employee_loan_views.xml',
        'views/account_move_views.xml',
        'views/menuitems.xml'
    ],
    
    'demo': [
    ],
}
