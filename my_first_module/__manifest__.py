# -*- coding: utf-8 -*-
{
    'name': "My First Module",

    'summary': """
        ODT TEST FOR SALE AND PURCHASE ORDER""",

    'description': """
        Adding new DP field in Purchase Order
        Adding new Total field in Purchase Order
        Create new function for difference price in Purchase Order
        Adding new different price field in Purchase Order
        Adding new Warehouse field in Sale Order
        Adding new boolean All Warehouse Field in Sale Order
        Changing the String Quotation to WIP Orders in Sale Order
        Changing the total price for each category with their line subtotal price
    """,

    'author': "Port Cities",
    'website': "https://www.portcities.net",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '13.0.2.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale', 'purchase', 'stock', 'product'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/purchase_order_views.xml',
        'views/sale_order_views.xml',
        'views/product_template_views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
}
