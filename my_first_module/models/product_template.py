from odoo import api, fields, models

class ProductTemplate(models.Model):
    _inherit = "product.template"
    # for storage group in view
    lower_limits = fields.Integer(String="Lower Limit")
    upper_limits = fields.Integer(String="Upper Limit")
    # for dimension group in view
    length = fields.Float(String="Length")
    width = fields.Float(String="Width")
    height = fields.Float(String="Height")
    volume = fields.Float(compute="_compute_volume", String="Volume")

    @api.depends('length', 'width', 'height')
    def _compute_volume(self):
        """For counting the colume with equation = w * l * h"""
        for records in self:
            records.volume = float(records.width) * float(records.length) * float(records.height)

    
    class ProductConstraints(models.Model):
        _inherit = 'product.product'
        @api.model
        def create(self, vals):
                
            res = super(ProductProduct, self).create(vals)
                
            # generate sequence
                
            ref = self.env.ref('webhook.seq_product_auto')
                
            res.default_code = ref.with_context(
                    
            force_company=False).next_by_code(
                    
            "product.product") or _('New')
                
            return super(ProductProduct, self).create(vals)
            
    class ProductConstraints(models.Model):
        _inherit = 'product.template'
        @api.model
        def create(self, vals_list):
        
            res = super(ProductConstraints, self).create(vals_list)
                
            if not res.barcode:
                    
                raise UserError(_("Add Barcode"))
                
            return res