
from odoo import api, fields, models

class PurchaseOrder(models.Model):
    _inherit = "purchase.order"
    dp = fields.Float(String="Down Payment")
    total = fields.Float(compute="_compute_total", store=True)
    email = fields.Char(related='partner_id.email')

    @api.depends('amount_total')
    def _compute_total(self):
        """Penjelesanan Function Disini"""
        for records in self:
            records.total = float(records.amount_total) - float(records.dp)

class PurchaseOrderLine(models.Model):
    _inherit = "purchase.order.line"

    diff_price = fields.Float(compute="_compute_diff_price")
    @api.depends('product_id')
    def _compute_diff_price(self):
        for records in self:
            records.diff_price = float(records.product_id.list_price) - float(records.product_id.standard_price)