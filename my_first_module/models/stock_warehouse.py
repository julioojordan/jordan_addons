from odoo import fields, models

class StockWarehouse(models.Model):
    _inherit = 'stock.warehouse'
    _description = 'For Saving the warehouse data'

    warehouse_id = fields.Many2one('sale.order', string="warehouse")