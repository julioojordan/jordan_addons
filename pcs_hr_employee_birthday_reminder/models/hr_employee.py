from odoo import fields, models
from dateutil.relativedelta import relativedelta


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    age = fields.Integer(String="Age")

    def get_web_base_url(self):
        web_base_url_id = self.env['ir.config_parameter'].search([("key", "=", "web.base.url")])
        return web_base_url_id.value

    def process_employee_birthday_scheduler(self):
        now = fields.datetime.now() + relativedelta(hours=7)
        month = str(now.month)
        day = str(now.day)
        year = now.year
        str_month = '0' + month if len(month) == 1 else month
        str_day = '0' + day if len(day) == 1 else day
        value = []
        query = """SELECT id
                        FROM hr_employee
                        WHERE EXTRACT(MONTH FROM birthday) =  %s AND EXTRACT(DAY FROM birthday) = %s"""
        value.append(str_month)
        value.append(str_day)
        self._cr.execute(query, value)
        record = self._cr.dictfetchall()
        ids = []
        for id_employee in record:
            ids.append(id_employee['id'])
        employee_ids = self.env['hr.employee'].browse(ids)
        print("XDDDDDDDDDD", employee_ids)

        if(len(employee_ids) == 0):
            print("No Employee Born Today")
        #Taking the email template Id Way 2
        else:
            mail_template = self.env.ref('pcs_hr_employee_birthday_reminder.email_template_birthday')

            #Sending Email for eaach employee born today
            for employee_id in employee_ids:
                #count employee year
                employee_birthday = str(employee_id.birthday).split("-")
                employee_year = year - int(employee_birthday[0])
                employee_id.write({'age' : employee_year})
                context = {
                    'email_to':employee_id.work_email,
                    'email_cc':employee_id.parent_id.work_email,
                    'employee_name': employee_id.name,
                    'employee_age': employee_id.age,
                    'employee_id': employee_id.id,
                    'web_bae_url': self.get_web_base_url(),
                }
                #mail_template.with_context(context).send_mail(self.id, force_send=True)
                print("EMAIL SENT to %s" %(employee_id.name))

    def tes_button(self):
        cron_id = self.env.ref('pcs_hr_employee_birthday_reminder.employee_birthday_scheduler')
        print('Next Call :', cron_id.nextcall+ relativedelta(hours=7))
        web_base_url_id = self.env['ir.config_parameter'].search([("key", "=", "web.base.url")])
        print('Web Base URl :', web_base_url_id.value)
        call = cron_id.nextcall
        now = fields.datetime.now()
        print('Current Date Time :', now)
        today = fields.Date.today()
        month = str(now.month)
        day = str(now.day)
        year = today.year
        str_month = '0' + month if len(month) == 1 else month
        str_day = '0' + day if len(day) == 1 else day
        value = []
        query = """SELECT id
                        FROM hr_employee
                        WHERE EXTRACT(MONTH FROM birthday) =  %s AND EXTRACT(DAY FROM birthday) = %s"""
        value.append(str_month)
        value.append(str_day)
        self._cr.execute(query, value)
        record = self._cr.dictfetchall()
        ids = []
        for id_employee in record:
            ids.append(id_employee['id'])
        employee_ids = self.env['hr.employee'].browse(ids)

        mail_template = self.env.ref('pcs_hr_employee_birthday_reminder.email_template_birthday')

        for employee_id in employee_ids:
            employee_birthday = str(employee_id.birthday).split("-")
            employee_year = year - int(employee_birthday[0])
            employee_id.write({'age' : employee_year})
            context = {
                'email_to':employee_id.work_email,
                'email_cc':employee_id.parent_id.work_email,
                'employee_name': employee_id.name,
                'employee_age': employee_id.age,
                'employee_id': employee_id.id,
                'web_bae_url': self.get_web_base_url(),
            }
            mail_template.with_context(context).send_mail(self.id, force_send=True)
            print("EMAIL SENT to %s" %(employee_id.name))

        #get Email template Id Way 1
        # template_id = self.env['ir.model.data'].get_object_reference('pcs_hr_employee_birthday_reminder', 'email_template_birthday')
        # template_rec = self.env['mail.template'].browse(template_id[1])

        # print("XDDDDDDDDDD", template_id)
        # print("XDDDDDDDDDD Template REC", template_rec)
        
        # #get Email template Id Way 2
        # mail_template = self.env.ref('pcs_hr_employee_birthday_reminder.email_template_birthday')
        # print("XDDDDDDDDDD Mail Template", mail_template)
        # # for temp_id in template_rec:
        # #     self.env['mail.template'].send_mail(temp_id.id, force_send = True)
        # #send data to email template

        # mail_template.write({
        #     'email_to': "andyanjordan1153@gmail.com",
        #     'body_html': "<h1>Happy th Birthday, Jordan</h1>",
        #     'body_html': "<p>Dear Test,</p><p>We wish you a happy th birthday, May your hopes and wishes for the past year have been fulfilled and in this year we are waiting for your new achievements!</p> <a href='%s/web#id=3&action=493&model=hr.employee&view_type=form&cids=1&menu_id=303'><button style='background-color: #4CAF50; border: none;color: white;padding: 15px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;'>Click Me</button></a>" %(web_base_url_id.value)
        # })
        # mail_template.send_mail(self.id, force_send=True)
        # for employee_id in employee_ids :
        #     print("XDDDDDDDDDD", employee_id.birthday)
        
        

    

        

    
