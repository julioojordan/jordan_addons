# -*- coding: utf-8 -*-
{
    'name': "PCS EMPLOYEE  BIRTHDAY REMINDER",

    'summary': """
        Custom Employee Birthday Reminder 
    """,

    'description': """
        Creating custom employe card pdf 
        Create a new employee card button
    """,

    'author': "Port Cities",
    'website': "https://www.portcities.net",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '13.0.1.0',

    # any module necessary for this one to work correctly
    'depends': ['base', 'hr', 'mail', 'contacts'],

    # always loaded
    'data': [
        'views/hr_employee_views.xml',
        'data/hr_employee_cron.xml',
        'data/employee_birthday_email_template.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
}
