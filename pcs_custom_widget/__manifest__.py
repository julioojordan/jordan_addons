# -*- coding: utf-8 -*-
{
    'name': "PCS Custom Widget Learning",

    'summary': """
        Customizing and Creating new widget learning""",

    'description': """
    """,

    'author': "Port Cities",
    'website': "https://www.portcities.net",

    'category': 'Uncategorized',
    'version': '13.0.1.0',

    'depends': ['web', 'sale', 'account'],

    'data': [
        'security/ir.model.access.csv',
        'views/widget_learning_views.xml',
        'views/sale_order_views.xml',
        'views/account_move_views.xml',
        'views/template.xml',
        'views/menuitems.xml'
    ],
    
    'qweb': [
        'static/src/xml/custom_widget_templates.xml',
    ],
    
    'demo': [],
}
