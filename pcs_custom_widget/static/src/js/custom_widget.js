odoo.define('pcs_custom_widget.custom_widget', function (require) {
    "use strict";
    var AbstractField = require('web.AbstractField');
    var FieldRegistry = require('web.field_registry');

    // import qweb to render a view
    var core = require('web.core');
    var qweb = core.qweb;
    

    //for customize datepicker
    var translation = require('web.translation');
    var _t = translation._t;
    var basic_fields = require('web.basic_fields');
    var field_utils = require('web.field_utils');
    var datepicker = require('web.datepicker');
    var time = require('web.time');

    // widget 1
    var FirstWidget = AbstractField.extend({
        template: 'FirstWidgetTemplate',
    });
 
    // widget 2
    var Widget2 = AbstractField.extend({
        template: 'Widget2Template',
        events: { // list of event using odoo javascript, like jquery event, we can use jquery event to in odoo javascript
            'click .btn-minus': 'btn_minus_action',
            'click .btn-plus': 'btn_plus_action',
        },
        btn_minus_action: function(){
            var new_value = this.value - 1;
            this._setValue(new_value.toString());            
        },
        btn_plus_action: function(){
            var new_value = this.value + 1;
            this._setValue(new_value.toString());
        },
        _render: function () {
            // re-render the view if the field value is changed
            this.$el.html($(qweb.render(this.template, {'widget': this})));
        },
    });

    // widget 3
    var Widget3 = AbstractField.extend({
        template: 'Widget2Template',
        events: { // list of event using odoo javascript, like jquery event, we can use jquery event to in odoo javascript
            'click .btn-minus': 'btn_minus_action',
            'click .btn-plus': 'btn_plus_action',
        },
        init: function () {
            this._super.apply(this, arguments);
            if(this.nodeOptions.step){
                // if user configure the 'step' value in xml file
                // change the default value to user desired value
                this.step = this.nodeOptions.step;
            }
        },
        btn_minus_action: function(){
            var new_value = this.value - this.step;
            this._setValue(new_value.toString());            
        },
        btn_plus_action: function(){
            var new_value = this.value + this.step;
            this._setValue(new_value.toString());
        },
        _render: function () {
            // re-render the view if the field value is changed
            this.$el.html($(qweb.render(this.template, {'widget': this})));
        },
    });

    field_utils.format.year = function formatDate(value, field, options) {
        if (value === false) {
            return "";
        }
        if (field && field.type === 'datetime') {
            if (!options || !('timezone' in options) || options.timezone) {
                value = value.clone().add(session.getTZOffset(value), 'minutes');
            }
        }
        var date_format = time.strftime_to_moment_format('%Y');
        return value.format(date_format);
    };

    field_utils.parse.year = function parseDate(value, field, options) {
        if (!value) {
            return false;
        }
        var datePattern;
        datePattern = time.strftime_to_moment_format('%Y')
        var datePatternWoZero = datePattern.replace('MM','M').replace('DD','D').replace('YYYY', 'Y');
        var date;
        if (options && options.isUTC) {
            date = moment.utc(value);
        } else {
            date = moment.utc(value, [datePattern, datePatternWoZero, moment.ISO_8601]);
        }
        if (date.isValid()) {
            if (date.year() === 0) {
                date.year(moment.utc().year());
            }
            if (date.year() >= 1900) {
                date.toJSON = function () {
                    return this.clone().locale('en').format('YYYY-MM-DD');
                };
                return date;
            }
        }
        throw new Error(_.str.sprintf(core._t("'%s' is not a correct date"), value));
    }

    datepicker.DateWidget.include({
        init: function (parent, options) {
            this._super(parent, _.extend({
                format : this.type_of_date === 'datetime' ? time.getLangDatetimeFormat() : this.type_of_date === 'year' ? time.strftime_to_moment_format('%Y') : time.getLangDateFormat(),
            }, options || {}));
        }
    });

    var DateYearWidget = datepicker.DateWidget.extend({
        type_of_date: "year",
    });

    basic_fields.FieldDate.include({
        _makeDatePicker: function () {
            if(this.nodeOptions.format_date){
                if (this.nodeOptions.format_date == 'year'){
                    return new DateYearWidget(this, this.datepickerOptions);
                }
            }else{
                return new datepicker.DateWidget(this, this.datepickerOptions);
            }
        },
    });
 
    // register the widget to web.field_registry object
    // so we can use our widget in odoo's view/xml file
    // with the code like below
    // <field name="field_one" widget="widget_one" />
    // the 'widget_one' name is up to you, as long as it's always connected/without spaces
    FieldRegistry.add('first_widget', FirstWidget);
    FieldRegistry.add('widget_2', Widget2);
    FieldRegistry.add('widget_3', Widget3);
 
    // return the widget object
    // so it can be inherited or overridden by another module
    return {
        FirstWidget,
        Widget2,
        Widget3,
        DateYearWidget
    };
});