from odoo import api, fields, models, _
from odoo.exceptions import UserError


class WidgetLearning(models.Model):
    _name = 'widget.learning'
    _description = 'model for widget learning'

    first_widget = fields.Integer(default=1)
    widget_2 = fields.Integer()
    widget_3 = fields.Integer(default=1)
    widget_4 = fields.Integer(default=1)
    widget_date_1 = fields.Date()
    widget_date_2 = fields.Date()
    widget_date_3 = fields.Datetime()

    @api.model
    def compare_vals(self, vals, id):
        record = self.browse([id])
        result = []
        same_vals = []
        if record.id:
            if vals:
                if record.widget_2 == vals['widget_2'] :
                    same_vals.append('Widget 2')
                if same_vals:
                    messages = ', '.join(map(str, same_vals))
                    txt = "same value detected on : " + messages
                    # result.append(txt)
                    # return result
                    raise UserError(_(txt))
                    # action = self.env.ref('crm.action_crm_lead2opportunity_partner').read()[0]
        if result:
            return False
        else:
            return True
