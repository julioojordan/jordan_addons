from odoo import api, fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    test_1 = fields.Integer(default=1)
    test_2 = fields.Integer()
    test_3 = fields.Integer(default=1)
    test_4 = fields.Integer(default=1)
    test_date_1 = fields.Date()