
from odoo import api, fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'
    _desciption = 'Giving new dereaft quotation state'

    state = fields.Selection(
        [('draft_quotation', 'Draft Quotation'),
        ('draft', 'Quotation'),
        ('sent', 'Quotation Sent'),
        ('sale', 'Sales Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled'),], 
        string='Status', 
        default='draft_quotation'
    )

    def action_approve(self):
        for records in self:
            """ Giving action to confirm button"""
            records.state = 'draft'

    
