from odoo import models


class SaleOrderReportXls(models.AbstractModel):
    _name = 'report.odt_xlsx_report.report_saleorder_xlsx'
    _inherit = 'report.odoo_report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, lines):
        """ Generating XLSX File """
        
        """Formating"""
        merge_format = workbook.add_format({'align': 'center'})
        date_format = workbook.add_format({'num_format': 'dd/mm/yyyy'})
        t_head = workbook.add_format({'align': 'center'})
        t_head.set_border()
        t_head.set_bold()
        t_body_1 = workbook.add_format({'align': 'center'})
        t_body_1.set_border()
        t_body_2 = workbook.add_format()
        t_body_2.set_border()
        bold = workbook.add_format({'bold': True})

        for record in lines:
            row = 8
            no = 1
            """ Generating Worksheet for each Sale Order """
            sheet = workbook.add_worksheet(record.name)

            """Write in the XLSX File"""
            sheet.merge_range('A1:E1', record.name, merge_format)
            
            sheet.write(2, 0, 'Customer')
            sheet.write(2, 2, record.partner_id.name)

            sheet.write(3, 0, 'Order Date')
            sheet.write_datetime(3, 2, record.date_order, date_format)

            sheet.write(4, 0, 'Sales Person')
            sheet.write(4, 2, record.user_id.name)

            sheet.write(6, 0, 'Order Lines')
            
            sheet.write(7, 0, 'No', t_head)
            sheet.write(7, 1, 'Name', t_head)
            sheet.write(7, 2, 'Qty', t_head)
            sheet.write(7, 3, 'Unit Price', t_head)
            sheet.write(7, 4, 'Subtotal', t_head)

            for records in record.order_line:
                sheet.write(row, 0, no, t_body_1)
                sheet.write(row, 1, records.product_id.name, t_body_2)
                sheet.write(row, 2, records.product_uom_qty, t_body_2)
                sheet.write(row, 3, records.price_unit, t_body_2)
                sheet.write(row, 4, records.price_subtotal, t_body_2)
                row += 1
                no += 1

            sheet.write(row+1, 3, 'Total', bold)
            sheet.write(row+1, 4, record.amount_total, bold)