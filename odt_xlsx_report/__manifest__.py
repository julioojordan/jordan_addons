# -*- coding: utf-8 -*-
{
    'name': "ODT Sales Order XLSX Reporting",

    'summary': """
        Custom XLSX Report using report_xls module
    """,

    'description': """
        Creating custom xlsx report based on requirement given
    """,

    'author': "Port Cities",
    'website': "https://www.portcities.net",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '13.0.1.0',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale'],

    # always loaded
    'data': [
        'report/sale_order_xlsx_report.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
}
