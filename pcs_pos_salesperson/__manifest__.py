# -*- coding: utf-8 -*-
{
    'name': "PCS POS SALESPERSON",

    'summary': """
        Customizing Odoo Point of Sale""",

    'description': """
        Adding cashier and sales person selection in pos view
        Adding clear button in the top of the numpad in pos view
        Customizing the style and logo of pos view
        Adding the sales_person_id field in pos.order & report.pos.order
        Adding grup by salesperson in reporting > order
    """,

    'author': "Port Cities",
    'website': "https://www.portcities.net",

    'category': 'Uncategorized',
    'version': '13.0.1.8',

    'depends': ['base', 'point_of_sale'],

    'data': [
        'views/point_of_sale_views.xml',
        'views/pos_order_views.xml',
        'views/report_pos_order_views.xml',
    ],
    
    'qweb': [
        'static/src/xml/point_of_sale_views.xml',
    ],
    
    'demo': [],
}
