odoo.define('pcs_pos_salesperson.point_of_sale_custom', function(require) {
	'use strict';
    // require pos screens
    var pos_screens = require('point_of_sale.screens');
    var chrome = require("point_of_sale.chrome");
    var PosBaseWidget = require('point_of_sale.BaseWidget');
    var core = require('web.core');
    var models = require('point_of_sale.models');
    var PosDB = require('point_of_sale.DB');
    var gui = require('point_of_sale.gui');

    console.log(gui)
    var _t = core._t;
    var _super_order = models.Order.prototype;
    
    chrome.Chrome.include({
        build_widgets: function(){
            this.widgets.push({
                'name':   'salesperson',
                'widget': SalesPersonWidget,
                'replace':  '.placeholder-SalesPersonWidget'});
            this._super();
        },
    });

    PosDB.include({ //kenapa ini load nya ndak PosDB.PosDb.include (tadi error malahan atua karena var nya cuman 1 di point_of_sale.db ya)
        get_sales_person: function() {
            return this.load('sales_person');
        },
        set_sales_person: function(sales_person) {
            this.save('sales_person', sales_person || null);
        }
    });

    models.PosModel = models.PosModel.extend({
        set_sales_person: function(employee){
            this.set('sales_person', employee);
            this.db.set_sales_person(this.get('sales_person'));
        },
        get_sales_person: function(){
            // reset the sales_person to the current user if session is new
            if (this.db.load('pos_session_id') !== this.pos_session.id) {
                this.set_sales_person(this.employee);
            }
            return this.db.get_sales_person() || this.get('sales_person') || this.employee;
        },
    });

    models.Orderline = models.Orderline.extend({

        get_product_image: function(){
            const product = this.product;
            return `/web/image?model=product.product&field=image_128&id=${product.id}&write_date=${product.write_date}&unique=1`;
        },
    
    });

    models.Order = models.Order.extend({
        export_as_JSON: function() {
            var data = _super_order.export_as_JSON.apply(this, arguments);
            data.sales_person_id = this.pos.get_sales_person().id
            return data;
        },
        export_for_printing: function(){
            var orderlines = [];
            var self = this;
    
            this.orderlines.each(function(orderline){
                orderlines.push(orderline.export_for_printing());
            });
    
            var paymentlines = [];
            this.paymentlines.each(function(paymentline){
                paymentlines.push(paymentline.export_for_printing());
            });
            var client  = this.get('client');
            var cashier = this.pos.get_cashier();
            var sales_person = this.pos.get_sales_person();
            var company = this.pos.company;
            var date    = new Date();
    
            function is_html(subreceipt){
                return subreceipt ? (subreceipt.split('\n')[0].indexOf('<!DOCTYPE QWEB') >= 0) : false;
            }
    
            function render_html(subreceipt){
                if (!is_html(subreceipt)) {
                    return subreceipt;
                } else {
                    subreceipt = subreceipt.split('\n').slice(1).join('\n');
                    var qweb = new QWeb2.Engine();
                        qweb.debug = config.isDebug();
                        qweb.default_dict = _.clone(QWeb.default_dict);
                        qweb.add_template('<templates><t t-name="subreceipt">'+subreceipt+'</t></templates>');
    
                    return qweb.render('subreceipt',{'pos':self.pos,'widget':self.pos.chrome,'order':self, 'receipt': receipt}) ;
                }
            }
    
            var receipt = {
                orderlines: orderlines,
                paymentlines: paymentlines,
                subtotal: this.get_subtotal(),
                total_with_tax: this.get_total_with_tax(),
                total_without_tax: this.get_total_without_tax(),
                total_tax: this.get_total_tax(),
                total_paid: this.get_total_paid(),
                total_discount: this.get_total_discount(),
                tax_details: this.get_tax_details(),
                change: this.get_change(),
                name : this.get_name(),
                client: client ? client.name : null ,
                invoice_id: null,   //TODO
                cashier: cashier ? cashier.name : null,
                sales_person: sales_person ? sales_person.name : null,
                precision: {
                    price: 2,
                    money: 2,
                    quantity: 3,
                },
                date: {
                    year: date.getFullYear(),
                    month: date.getMonth(),
                    date: date.getDate(),       // day of the month
                    day: date.getDay(),         // day of the week
                    hour: date.getHours(),
                    minute: date.getMinutes() ,
                    isostring: date.toISOString(),
                    localestring: this.formatted_validation_date,
                },
                company:{
                    email: company.email,
                    website: company.website,
                    company_registry: company.company_registry,
                    contact_address: company.partner_id[1],
                    vat: company.vat,
                    vat_label: company.country && company.country.vat_label || _t('Tax ID'),
                    name: company.name,
                    phone: company.phone,
                    logo:  this.pos.company_logo_base64,
                },
                currency: this.pos.currency,
            };
    
            if (is_html(this.pos.config.receipt_header)){
                receipt.header = '';
                receipt.header_html = render_html(this.pos.config.receipt_header);
            } else {
                receipt.header = this.pos.config.receipt_header || '';
            }
    
            if (is_html(this.pos.config.receipt_footer)){
                receipt.footer = '';
                receipt.footer_html = render_html(this.pos.config.receipt_footer);
            } else {
                receipt.footer = this.pos.config.receipt_footer || '';
            }
            return receipt;
        }
    });

    gui.Gui.include({
        select_sales_person: function(options) {
            options = options || {};
            var self = this;

            var list = [];
            this.pos.employees.forEach(function(sales_person) {
                if (!options.only_managers || sales_person.role === 'manager') {
                    list.push({
                    'label': sales_person.name,
                    'item':  sales_person,
                    });
                }
            });
    
            var prom = new Promise(function (resolve, reject) {
                self.show_popup('selection', {
                    title: options.title || _t('Select User'),
                    list: list,
                    confirm: resolve,
                    cancel: reject,
                    is_selected: function (sales_person) {
                        return sales_person === self.pos.get_sales_person();
                    },
                });
            });
    
            return prom.then(function (sales_person) {
                return self.ask_password(sales_person.pin).then(function(){
                    return sales_person;
                });
            });
        },
    });    
    var SalesPersonWidget = PosBaseWidget.extend({
        template: 'SalesPersonWidget',
        init: function(parent, options){
            options = options || {};
            this._super(parent,options);
        },
        renderElement: function(){
            var self = this;
            this._super();
            this.$el.click(function(){
                self.click_username();
            });
        },
        click_username: function(){
            if(!this.pos.config.module_pos_hr) { return; }
            var self = this;
            //this.gui.select_employee({
            this.gui.select_sales_person({
                'security':     true,
                'current_employee': this.pos.get_sales_person(),
                'title':      _t('Change Salesperson'),
            }).then(function(employee){
                self.pos.set_sales_person(employee);
                self.chrome.widget.salesperson.renderElement();
                self.renderElement();
            });
        },
        get_name: function(){
            var user = this.pos.get_sales_person();
            if(user){
                return user.name;
            }else{
                return "";
            }
        },
    });
    
    // create a new button by extending the base ActionButtonWidget
    // for clearing the current order
    var ClearOrderButton = pos_screens.ActionButtonWidget.extend({
        template: 'ClearOrderButton',
        deleteorder_click_handler: function() {
            var self  = this;
            var order = this.pos.get_order(); 
            if (!order) {
                return;
            } else if ( !order.is_empty() ){
                self.pos.delete_current_order();
            } else {
                this.pos.delete_current_order();
            }
        },
        button_click: function(){
            this.deleteorder_click_handler();
        },
    });
    
    // define the clear order button
    pos_screens.define_action_button({
        'name': 'ClearOrder',
        'widget': ClearOrderButton,
        'condition': function(){return this.pos;},
    });

    // var NumpadWidget = PosBaseWidget.extend({
    //     template:'NumpadWidget',
    //     init: function(parent) {
    //         this._super(parent);
    //         this.state = new models.NumpadState();
    //     },
    //     changedMode: function() {
    //         var mode = this.state.get('mode');
    //         alert("Dashboard button clicked");
    //         $('.selected-mode').removeClass('selected-mode');
    //         $(_.str.sprintf('.mode-button[data-mode="%s"]', mode), this.$el).addClass('selected-mode');
    //     },
    // });

    // chrome.Chrome.include({
    //     build_widgets: function(){
    //         this.widgets.push({
    //             'name':   'shopname',
    //             'widget': ShopnameWidget,
    //             'replace':  '.placeholder-ShopnameWidget'});
    //         this._super();
    //     },
    // });

    // var ShopnameWidget = PosBaseWidget.extend({
    //     template: 'ShopnameWidget',
    //     init: function(parent, options){
    //         options = options || {};
    //         this._super(parent,options);
    //     },
    //     renderElement: function(){
    //         var self = this;
    //         this._super();
    //     },
    //     get_shopname: function(){
    //         var branch = this.pos.pos_session.branch_id;
    //         if(branch){
    //             return branch[1];
    //         }else{
    //             return "";
    //         }
    //     },
    // });
    // return {
    //         'ShopnameWidget': ShopnameWidget,
    //     };

    // return {
    //     'SalesPersonWidget': SalesPersonWidget,
    // };
});