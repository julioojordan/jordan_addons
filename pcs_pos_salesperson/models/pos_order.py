from odoo import api, fields, models


class PosOrder(models.Model):
    _inherit = 'pos.order'

    sales_person_id = fields.Many2one('hr.employee')
    # sales_person_id = fields.Integer()

    @api.model
    def _order_fields(self, ui_order):
        order = super(PosOrder, self)._order_fields(ui_order)
        order['sales_person_id'] = ui_order.get('sales_person_id', False)
        print("XDDD", ui_order.get('sales_person_id', False))
        return order