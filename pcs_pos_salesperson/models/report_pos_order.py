from odoo import fields, models


class ReportPosOrder(models.Model):
    _inherit = 'report.pos.order'

    sales_person_id = fields.Many2one('hr.employee')

    def _select(self):
        return super(ReportPosOrder, self)._select() + ',s.sales_person_id AS sales_person_id'

    def _group_by(self):
        return super(ReportPosOrder, self)._group_by() + ',s.sales_person_id'