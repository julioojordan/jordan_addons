odoo.define("barcode_action.form", function(require) {
    "use strict";

    var FormController = require("web.FormController");

    FormController.include({
        _barcodeHandleAction: function(barcode) {
            var record = this.model.get(this.handle);
            var self = this;
            console.log("Model Data :", record.data.model);
            console.log("Method :", record.data.method);
            console.log("barcode :", barcode);
            self
                ._rpc({
                    model: record.data.model,
                    method: record.data.method,
                    args: [[record.data.res_id], barcode],
                })
                .then(function(action) {
                    if (action) {
                        self._barcodeStopListening();
                        self.do_action({
                            type: 'ir.actions.act_window',
                            res_model: 'hr.employee',
                            name: 'model_hr_employee',
                            views: [[false, 'form']],
                            res_id: action,
                        });
                    }
                });
        },
        _barcodeActiveScanned: function (method, barcode, activeBarcode) {
            var self = this;
            var methodDef;
            var def = new Promise(function (resolve, reject) {
                if (typeof method === 'string') {
                    methodDef = self[method](barcode, activeBarcode);
                } else {
                    methodDef = method.call(self, barcode, activeBarcode);
                }
                if (methodDef){
                    methodDef
                    .then(function () {
                        var record = self.model.get(self.handle);
                        var candidate = self._getBarCodeRecord(record, barcode, activeBarcode);
                        activeBarcode.candidate = candidate;
                    })
                    .then(resolve, resolve);
                }
            });
            return def;
        }
    });
});
