
from odoo import api, fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'
    _desciption = 'Adding down payment field'


    down_payment = fields.Monetary()

    @api.depends('down_payment', 'order_line.price_total')
    def _amount_all(self):
        """counting amount_total by override _amount_all() function"""
        res = super(SaleOrder, self)._amount_all()
        for records in self:
            records.amount_total = float(records.amount_tax) + float(records.amount_untaxed) - float(records.down_payment)
        return res

    
