# -*- coding: utf-8 -*-
{
    'name': "ODT Sales Order PDF Reporting",

    'summary': """
        Custom PDF Report""",

    'description': """
        Adding field down payment
        Adding product type, down payment, and re-calculate total amount in the existing sale pdf report
    """,

    'author': "Port Cities",
    'website': "https://www.portcities.net",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '13.0.1.0',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale'],

    # always loaded
    'data': [
        'views/sale_order_report_views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
}
