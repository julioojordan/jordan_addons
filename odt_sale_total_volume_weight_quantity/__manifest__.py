# -*- coding: utf-8 -*-
{
    'name': "ODT Sales Order Totaling Volume, Weight, and Quantity",

    'summary': """
        Assessment Task for Coding Quality Task 3""",

    'description': """
        Adding field volume, weight, quantity
    """,

    'author': "Port Cities",
    'website': "https://www.portcities.net",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '13.0.1.0',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale_stock'],

    # always loaded
    'data': [
        'views/sale_order_views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
}
