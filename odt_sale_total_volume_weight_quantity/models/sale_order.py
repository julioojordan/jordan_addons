
from odoo import api, fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    total_volume = fields.Float(compute="_compute_total_volume")
    total_weight = fields.Float(compute="_compute_total_weight")
    total_quantity = fields.Float(compute="_compute_total_quantity")

    @api.depends('order_line')
    def _compute_total_volume(self):
        """compute total volume function"""
        for records in self:
            if (len(records.order_line) != 0):
                records.total_volume = 0
                for line in records.order_line:
                    records.total_volume += float(line.product_id.volume) * float(line.product_uom_qty)
            else:
                records.total_volume = 0

    @api.depends('order_line')
    def _compute_total_weight(self):
        """compute total weight"""
        for records in self:
            if (len(records.order_line) != 0):
                records.total_weight = 0
                for line in records.order_line:
                    records.total_weight += float(line.product_id.weight) * float(line.product_uom_qty)
            else:
                records.total_weight = 0

    @api.depends('order_line')
    def _compute_total_quantity(self):
        """compute total quantity"""
        for records in self:
            if (len(records.order_line) != 0):
                records.total_quantity = 0
                for line in records.order_line:
                    records.total_quantity += float(line.product_uom_qty)
            else:
                records.total_quantity = 0

    
