import json

from odoo import _, models
from odoo.tools.safe_eval import safe_eval

class HrEmployee(models.Model):
    _inherit = 'hr.employee'
    _desciption = 'Giving new print employee card button and barcode scanner action'

    def action_print_card(self):
        data = {
            'employee_id': self.id
        }
        return self.env.ref('pcs_hr_employee_card.action_employee_card').report_action(self, data=data)
        
    def find_employee_using_barcode(self, barcode):
        employee = self.search([("barcode", "=", barcode)], limit=1)
        if not employee:
            action = self.env.ref("pcs_hr_employee_card.hr_employee_find")
            result = action.read()[0]
            context = safe_eval(result["context"])
            context.update(
                {
                    "default_state": "warning",
                    "default_status": _(
                        "Employee with Barcode " "%s cannot be found"
                    )
                    % barcode,
                }
            )
            result["context"] = json.dumps(context)
            return result
        # action = self.env.ref("hr.view_employee_form")
        # print("Action yang didapat:", action)
        # result = action.read()[0]
        res = self.env.ref("hr.view_employee_form", False)
        # result["views"] = [(res and res.id or False, "form")]
        # result["res_id"] = employee.id
        # print("Result views yang didapat:", result["views"])
        # print("Res model yang didapat:", res.name)
        return {
                'type': 'ir.actions.act_window',
                'res_model': res.model,
                'name': 'model_hr_employee',
                'views': [[res and res.id or False, "form"]],
                'res_id': employee.id,
            }
    
