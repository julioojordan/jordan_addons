from odoo import api, models


class HrEmployee(models.AbstractModel):
    _name = 'report.pcs_hr_employee_card.employee_card'
    
    @api.model
    def _get_report_values(self, docids, data):
        """in this function can access the data returned from the button
        click function"""
        model = self.env.context.get('active_model')
        data_employee = self.env[model].browse(self.env.context.get('active_id'))

        print("Current Model:", model)
        print("Current ID:", data_employee)
        model_id = data['employee_id']
        image_url = "data:image/*;base64,/web/content/hr.employee/"+str(model_id)+"/image_1920" 
        value = []
        data_employee = self.env['hr.employee'].browse(model_id)
        # query = """SELECT he.id as id, he.name as name, 
        #                 he.work_phone as wp, he.barcode as barcode,
        #                 he.work_email as email, 
        #                 hd.name as hd_name, hj.name as hj_name
        #                 FROM hr_employee as he
        #                 JOIN hr_department AS hd ON he.department_id = hd.id
        #                 JOIN hr_job AS hj ON he.job_id = hj.id
        #                 WHERE he.id = %s"""
        # value.append(model_id)
        # self._cr.execute(query, value)
        # record = self._cr.dictfetchall()
        # record.append((0, 0, {
        #         'image':  image_url
        #     }))
        return {
            'record': data_employee
        }
        