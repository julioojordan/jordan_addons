# -*- coding: utf-8 -*-
{
    'name': "PCS CUSTOM EMPLOYEE CARD",

    'summary': """
        Custom Employee Card 
    """,

    'description': """
        Creating custom employe card pdf 
        Create a new employee card button
        Create barcode action
    """,

    'author': "Port Cities",
    'website': "https://www.portcities.net",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '13.0.1.3',

    # any module necessary for this one to work correctly
    'depends': ['base', 'hr', 'barcodes'],

    # always loaded
    'data': [
        'report/hr_employee_card.xml',
        'views/hr_employee_views.xml',
        'views/barcode_templates.xml',
        'wizard/barcode_action_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
}
