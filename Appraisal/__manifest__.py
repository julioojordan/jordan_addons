# -*- coding: utf-8 -*-
{
    'name': "Appraisal",

    'summary': """
        First Appraisal""",

    'description': """
        Appraisal
    """,

    'author': "Port Cities",
    'website': "https://www.portcities.net",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '13.0.1.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale', 'purchase'],

    # always loaded
    'data': [
        'views/purchase_order_views.xml',
        # 'views/sale_order_views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
}
