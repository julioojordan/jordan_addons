odoo.define('pcs_pos_shortcut.point_of_sale_custom_shortcut', function(require) {
	'use strict';
    // require pos screens
    var pos_screens = require('point_of_sale.screens');
    var models = require('point_of_sale.models');
    
    var _super_posmodel = models.PosModel.prototype;
    

    // var isKeyPressed = {
    //     a: false, // ASCII code for 'a'
    //     b: false // ASCII code for 'b'
    //     // ... Other keys to check for custom key combinations
    // };
    
    // document.onkeydown = keyDownEvent => {
    //     //Prevent default key actions, if desired
    //     //keyDownEvent.preventDefault();
    //     var self = this; 
    //     // Track down key click
    //     isKeyPressed[keyDownEvent.key] = true;
    //     console.log(keyDownEvent.key)

    //     // Check described custom shortcut
    //     if (isKeyPressed["q"]) {
    //         self.Gui.show_screen('clientlist')
    //     }
    // };

    // document.onkeyup = keyUpEvent => {
    //     // Prevent default key actions, if desired
    //     //keyUpEvent.preventDefault();

    //     // Track down key release
    //     isKeyPressed[keyUpEvent.key] = false;
    // };

    
    // Static Shortcut button for customer using 'q' with key id =81
    pos_screens.ActionpadWidget.include({
        renderElement: function() {
            var self = this;
            this._super();
            $(document).keydown(function (e) {
                // var inputValue = $(this).val(); 
                console.log(e.keyCode)
                if(e.keyCode == 81) {
                    self.gui.show_screen('clientlist');
                }
            });
        }
    });

    // adding shortcut field in the models.js
    models.PosModel = models.PosModel.extend({
        initialize: function (session, attributes) {
            // New code
            var partner_model = _.find(this.models, function(model){
                return model.model === 'pos.payment.method';
            });
            partner_model.fields.push('shortcut');
    
            // Inheritance
            return _super_posmodel.initialize.call(this, session, attributes);
        }
    });

    // dynamic shortcut button
    pos_screens.PaymentScreenWidget.include({
        renderElement: function() {
            var self = this;
            console.log(this.pos.config.id)
            this._super();
            var shortcut_data = [];
            self._rpc({
                model: 'pos.config',
                method: 'get_shortcut',
                args: [this.pos.config.id],
            })
            .then(function(datas) {
                console.log(datas)
                _.each(datas, function(data) {
                    //shortcut_data.push(data.name)
                    console.log("Data ID :", data.id);
                    console.log("Data Cde :", data.code);
                    if (data.code){
                        $(document).keydown(function (e) {
                            e.preventDefault()
                            if(e.keyCode == parseInt(data.code)) {
                                self.click_paymentmethods(data.id);
                            }
                        });
                    }
                });
            });
        }
    });
});