from odoo import fields, models


class PosShortcutConfig(models.Model):
    _name = 'pos.shortcut.config'
    _description = 'Database for shortcut config'

    name = fields.Char(String="Name")
    code = fields.Char(String="code")