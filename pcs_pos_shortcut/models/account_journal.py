from odoo import fields, models


class AccountJournal(models.Model):
    _inherit = 'account.journal'
    _description = 'Adding M2O field for shortcut'

    shortcut_payment_id = fields.Many2one('pos.shortcut.config')

    # @api.onchange('shortcut_payment_id')
    # def _change_sc_name(self):
    #     """Changing the name in pos payment method if we change the shortcut payment id in account journal"""
    #     if self.shortcut_payment_id:
    #         method_ids = self.env['pos.payment.method'].search([('cash_journal_id', 'like', self.id)])
    #         print("Self ID;", self.id)
    #         print("Pos Payment Method;", method_ids)
    #         for method_id in method_ids:
    #             method_id.update({
    #                 'shortcut': self.shortcut_payment_id.name or False
    #             })