# -*- coding: utf-8 -*-

from . import pos_shortcut_config
from . import account_journal
from . import pos_payment_method
from . import pos_config