from odoo import api, fields, models


class PosPaymentMethod(models.Model):
    _inherit = 'pos.config'
    _description = 'inherit pos config for taking the pos payment method ids'

    @api.model
    def get_shortcut(self, config_id):
        config_ids = self.browse(config_id)
        data=[]
        for method_id in config_ids.payment_method_ids:
            data.append({
                "id": method_id.id, 
                "code": method_id.cash_journal_id.shortcut_payment_id.code if method_id.cash_journal_id else False
            })
        return data