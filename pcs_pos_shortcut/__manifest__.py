# -*- coding: utf-8 -*-
{
    'name': "PCS POS SHORTCUT",

    'summary': """
        Customizing Odoo Point of Sale Keyboard Shortcut""",

    'description': """
        Adding Static shortcut in pos for customer by pressing key 'q'
        Adding menuitems for shortcut setting
        adding shortcut setting view
    """,

    'author': "Port Cities",
    'website': "https://www.portcities.net",

    'category': 'Uncategorized',
    'version': '13.0.1.2',

    'depends': ['base', 'point_of_sale', 'account'],

    'data': [
        'security/ir.model.access.csv',
        'views/pos_shortcut_config_views.xml',
        'views/point_of_sale_views.xml',
        'views/test.xml',
        'views/account_journal_views.xml'
    ],
    
    'qweb': [
        'static/src/xml/point_of_sale_views.xml',
    ],
    
    'demo': [],
}
