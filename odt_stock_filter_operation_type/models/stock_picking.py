
from odoo import fields, models


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    type_of_operation = fields.Selection(
        [('incoming', 'Receipt'),
        ('internal', 'Internal Transfer'),
        ('outgoing', 'Delivery'),],
        related="picking_type_id.code",
        store=True
    )
    

    
