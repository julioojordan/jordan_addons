# -*- coding: utf-8 -*-
{
    'name': "ODT Picking Filter",

    'summary': """
        Assessment Task for Coding Quality Task 5""",

    'description': """
        Adding new type_of_operation fields
        Adding filter by operation type in picking
        Adding group by operation type in picking
    """,

    'author': "Port Cities",
    'website': "https://www.portcities.net",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '13.0.1.0',

    # any module necessary for this one to work correctly
    'depends': ['base', 'stock'],

    # always loaded
    'data': [
        'views/stock_picking_views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
}
