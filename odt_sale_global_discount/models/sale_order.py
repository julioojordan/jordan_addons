
from odoo import api, fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'
    _desciption = 'Adding total global discount and overriding _amount_all function'


    global_discount = fields.Float()
    total_global_discount = fields.Float(compute="_compute_total_global_discount", store=True)

    @api.depends('global_discount', 'amount_untaxed')
    def _compute_total_global_discount(self):
        for records in self:
            """counting total global discount"""
            records.total_global_discount = float(records.amount_untaxed) * float(records.global_discount) / 100
    
    @api.depends('total_global_discount', 'order_line.price_total')
    def _amount_all(self):
        """counting amount_total by override _amount_all() function"""
        res = super(SaleOrder, self)._amount_all()
        for records in self:
            records.amount_total = float(records.amount_tax) + float(records.amount_untaxed) - float(records.total_global_discount)
        return res

    
