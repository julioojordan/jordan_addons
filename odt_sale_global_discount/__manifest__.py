# -*- coding: utf-8 -*-
{
    'name': "ODT Sales Order Global Discount",

    'summary': """
        Assessment Task for Coding Quality Task 4""",

    'description': """
        Adding field global discount
        Adding field total global discount
        Overriding amount_all function with the new one
    """,

    'author': "Port Cities",
    'website': "https://www.portcities.net",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '13.0.1.2',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale', 'my_first_module'],

    # always loaded
    'data': [
        'views/sale_order_views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
}
