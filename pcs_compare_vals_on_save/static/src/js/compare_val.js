odoo.define('pcs_compare_vals_on_save.compare_val', function (require) {
    "use strict";

    var FormController = require('web.FormController');
    var core = require('web.core');
    var qweb = core.qweb;

    //add all the models you want to compare the values ​​here
    var used_in_model = ["widget.learning"];

    FormController.include({
        renderButtons: function ($node) {
            var $footer = this.footerToButtons ? this.renderer.$('footer') : null;
            var mustRenderFooterButtons = $footer && $footer.length;
            if (!this.defaultButtons && !mustRenderFooterButtons) {
                return;
            }
            this.$buttons = $('<div/>');
            if (mustRenderFooterButtons) {
                this.$buttons.append($footer);
    
            } else {
                this.$buttons.append(qweb.render("FormView.buttons", {widget: this}));
                this.$buttons.on('click', '.o_form_button_edit', this._onEdit.bind(this));
                this.$buttons.on('click', '.o_form_button_create', this._onCreate.bind(this));
                this.$buttons.on('click', '.o_form_button_save', this._onSaveCustom.bind(this));
                this.$buttons.on('click', '.o_form_button_cancel', this._onDiscard.bind(this));
                this._assignSaveCancelKeyboardBehavior(this.$buttons.find('.o_form_buttons_edit'));
                this.$buttons.find('.o_form_buttons_edit').tooltip({
                    delay: {show: 200, hide:0},
                    title: function(){
                        return qweb.render('SaveCancelButton.tooltip');
                    },
                    trigger: 'manual',
                });
                this._updateButtons();
            }
            this.$buttons.appendTo($node);
        },
        _onSaveCustom: function (ev) {
            ev.stopPropagation(); // Prevent x2m lines to be auto-saved
            var self = this;
            var record = this.model.get(this.handle);
            if (record.model && used_in_model.includes(record.model) ){
                this._rpc({
                    model: record.model,
                    method: 'compare_vals',
                    args: [record.data, record.data.id],
                }).then(function(action) {
                    if (!action) {
                        self._enableButtons();
                    }else{
                        self._disableButtons();
                        self.saveRecord().then(self._enableButtons.bind(self)).guardedCatch(self._enableButtons.bind(self));
                    }
                });
            }else{
                self._disableButtons();
                self.saveRecord().then(self._enableButtons.bind(self)).guardedCatch(self._enableButtons.bind(self));
            }
        },
        // saveRecord: function () {
        //     var record = this.model.get(this.handle);
        //     var val_list = [];
        //     console.log(this);
        //     // $.each(record.data, function(key,val){
        //     //     val_list.push({title : link});
        //     // });
        //     // for (let i = 0; i <= Object.keys(record.data).length; i++) {
        //     //     var kunci = Object.keys(record.data)[i];
        //     //     var value = record.data[Object.keys(record.data)[i]];
        //     //     val_list.push({kunci : value});
        //     // }
        //     console.log(record.data);
        //     // console.log(record.data[Object.keys(record.data)[0]]);
        //     // console.log(Object.keys(record.data)[1]);
        //     if (record.context.params.model){
        //         this._rpc({
        //             model: record.context.params.model,
        //             method: 'compare_vals',
        //             args: [record.data, record.data.id],
        //         }).then(function(action) {
        //             var self = this;
        //             // console.log(action)
        //             console.log(self);
        //             self.do_notify("Error",action,true);
        //             return this._super.apply(this, arguments);
        //             // if (!action) {
        //             //     return this._super.apply(this, arguments);
        //             //     // self.do_action({
        //             //     //     type: 'ir.actions.act_window',
        //             //     //     res_model: record.context.params.model,
        //             //     //     name: 'model_widget_learning',
        //             //     //     views: [[false, 'form']],
        //             //     //     res_id: record.data.id,
        //             //     //     mode:'edit'
        //             //     // });
        //             // }else{
        //             //     console.log(action)
        //             //     this.do_notify("Error",action,true);
        //             // }
        //         });
        //     }
        //     return this._super.apply(this, arguments);
        //     // return this._super();
        // },
    });
});