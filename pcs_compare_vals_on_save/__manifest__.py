# -*- coding: utf-8 -*-
{
    'name': "PCS Custom Compare value before saving data",

    'summary': """
        Customizing save button action""",

    'description': """
        Customizing save button action so that it will compare some fields value first before doing the save action
    """,

    'author': "Port Cities",
    'website': "https://www.portcities.net",

    'category': 'Uncategorized',
    'version': '13.0.1.0',

    'depends': ['web'],

    'data': [
        'views/template.xml'
    ],
    
    'demo': [],
}
