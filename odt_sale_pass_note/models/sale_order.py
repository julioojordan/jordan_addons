from odoo import api, fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'
    _desciption= 'override confirrm function for sending sale order not to delivery order'

    def action_confirm(self):
        """override the action_confirm() function"""
        res = super(SaleOrder, self).action_confirm()
        for records in self.picking_ids:
            records.write({'note' : self.note})
        return res