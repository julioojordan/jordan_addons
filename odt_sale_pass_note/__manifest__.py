# -*- coding: utf-8 -*-
{
    'name': "ODT Pasing Note to Deluvery Order",

    'summary': """
        Assessment Task for Coding Quality Task 2""",

    'description': """
        Sending sales order note to delivery order note
    """,

    'author': "Port Cities",
    'website': "https://www.portcities.net",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '13.0.1.3',

    # any module necessary for this one to work correctly
    'depends': ['base', 'stock', 'sale'],

    # always loaded
    'data': [],
    # only loaded in demonstration mode
    'demo': [],
}
