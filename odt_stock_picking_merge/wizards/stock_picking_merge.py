#import time

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class StockPickingMerge(models.TransientModel):
    _name = "stock.picking.merge"
    _description = "Merge picking transfer wizard pop up" 

    date_action = fields.Datetime(default=lambda self: fields.datetime.now())
    merge_ids = fields.Many2many('stock.picking')

    @api.model
    def default_get(self, fields):
        res = super(StockPickingMerge, self).default_get(fields)
        merge_transfer_ids = self.env['stock.picking'].browse(self._context.get('active_ids'))
        res.update({
            'merge_ids': [(6, 0, merge_transfer_ids.ids)]
        })
        print("Active Ids", merge_transfer_ids)
        print("fields list", fields)
        print("Res", res)
        return res

    def validation(self, merge_transfer_ids):
        """checking the merge picking transfer requirements"""
        counter = 0

        """checking if only one picking selected""" 
        if (len(self._context.get('active_ids', [])) < 2):
            raise UserError(_('Can not merge one single picking'))
          
        for transfer in merge_transfer_ids:
            """checking if there are pickings with done satus are selected"""
            if (transfer.state == 'done'):
                raise UserError(_('Can not merge already done pickings'))

            """checking if pickings are in ready state"""
            
            if (transfer.move_line_ids_without_package.product_uom_id.id == False):
                raise UserError(_('Can merge only pickings that are ready (state)'))

            if(counter == 0):
                temp_partner_id = transfer.partner_id.id
                partner_id = transfer.partner_id.id
                temp_picking_type_id = transfer.picking_type_id
                picking_type_id = transfer.picking_type_id
            else:
                partner_id = transfer.partner_id.id
                picking_type_id = transfer.picking_type_id
                """checking if partner of all pickings selected are the same""" 
                if (temp_partner_id != partner_id):
                    raise UserError(_('Can not merge different partners picking'))

                """checking if operation type of all pickings selected are the same"""
                if (temp_picking_type_id != picking_type_id):
                    raise UserError(_('Can merge only with the same operation type picking'))
            counter += 1    

    def create_merge_transfer(self):
        """ Function to creat a merge transfer picking """
        merge_transfer_ids = self.env['stock.picking'].browse(self._context.get('active_ids', []))

        """checking the requirement"""
        self.validation(merge_transfer_ids)

        """asigning array"""
        move_ids_without_package = []
        move_line_ids_without_package = []

        """origin is for for setting the source documents"""
        origin = ""
        counter = 0
        for transfer in merge_transfer_ids:
            partner_id = transfer.partner_id
            picking_type_id = transfer.picking_type_id
            location_id = transfer.location_id
            state = transfer.state

            """filling source document (origin field)"""
            if (counter == 0):
                origin += transfer.name
            else :
                origin = origin + " - " +transfer.name
            
            """Check if it's an internal transfer"""
            if ((transfer.picking_type_id.default_location_src_id.id == transfer.picking_type_id.default_location_dest_id.id) and (transfer.picking_type_id.code == 'internal')):
                location_dest_id = transfer.location_dest_id.id
            else:
                location_dest_id = location_id.id
            
            move_ids_without_package.append((0, 0, {
                'product_id':  transfer.move_ids_without_package.product_id.id,
                'product_uom_qty':transfer.move_ids_without_package.product_uom_qty,
                'product_uom':transfer.move_ids_without_package.product_uom.id,
                'location_id':transfer.move_ids_without_package.location_id.id,
                'location_dest_id':transfer.move_ids_without_package.location_dest_id.id,
                'date_expected':fields.datetime.now(),
                'quantity_done':transfer.move_ids_without_package.quantity_done,
                'reserved_availability':transfer.move_ids_without_package.reserved_availability,
                'date':fields.datetime.now(),
                'state':transfer.move_ids_without_package.state,
                'name':transfer.move_ids_without_package.name,
                'company_id':transfer.move_ids_without_package.company_id.id
            }))
            move_line_ids_without_package.append((0, 0, {
                'product_id':  transfer.move_line_ids_without_package.product_id.id,  
                'location_id':transfer.move_line_ids_without_package.location_id.id,
                'lot_id':transfer.move_line_ids_without_package.lot_id.id,
                'product_uom_qty':transfer.move_line_ids_without_package.product_uom_qty,
                'location_dest_id':transfer.move_line_ids_without_package.location_dest_id.id,
                'date':fields.datetime.now(),
                'qty_done':transfer.move_line_ids_without_package.qty_done,
                'product_uom_id':transfer.move_line_ids_without_package.product_uom_id.id,
                'company_id':transfer.move_line_ids_without_package.company_id.id
            }))
            counter += 1

        vals = {
            'partner_id':partner_id.id,
            'picking_type_id':picking_type_id.id,
            'location_id':location_id.id,
            'location_dest_id':location_dest_id,
            'origin':origin,
            'state':state,
            'scheduled_date':self.date_action,
            'move_ids_without_package':move_ids_without_package,
            'move_line_ids_without_package':move_line_ids_without_package,
            'is_merged_picking': True
        }
        self.env['stock.picking'].create(vals)
        merged_pickings = self.env['stock.picking'].search([("is_merged_picking", "=", True)])
        for records in merged_pickings:
            origins = records.origin.split(" - ")
            for data in origins:
                find_records = self.env['stock.picking'].search([("name", "=", data)], limit=1)

                """reset to draft already merge picking"""
                find_records.state = 'draft'

                """ give related fields to the record"""
                find_records.merged_picking_id = records.id
                find_records.is_done_merged_picking = True