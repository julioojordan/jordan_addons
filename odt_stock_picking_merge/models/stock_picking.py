
from odoo import api, fields, models


class StockPicking(models.Model):
    _inherit = 'stock.picking'
    _description = 'adding some fields to stock picking for merge transfer purpose'

    is_merged_picking = fields.Boolean(default = False, store = True) #for checking if the record is a merge transfer result or not
    is_done_merged_picking = fields.Boolean(default = False, store = True)#for checking if the record has a merge transfer or not
    merged_picking_id = fields.Many2one('stock.picking', store = True, string = 'Merged Picking')
    merged_picking = fields.Char(related='merged_picking_id.name')
    
