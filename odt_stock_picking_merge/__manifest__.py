# -*- coding: utf-8 -*-
{
    'name': "ODT Merge Picking Transfer",

    'summary': """
        Custom Merge Picking""",

    'description': """
        Adding Merge Picking in action dropdown 
        Auto create draft record bug fixed on pop up wizard
        Reset to draft merged stock pickings
        Adding refereence merge picking id to the merged picking
    """,

    'author': "Port Cities",
    'website': "https://www.portcities.net",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '13.0.1.3',

    # any module necessary for this one to work correctly
    'depends': ['base', 'stock'],

    # always loaded
    'data': [
        'views/stock_picking_views.xml',
        'wizards/stock_picking_merge_views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
}
